# 检查配置

- 检查IDEA的maven配置文件和仓库有没有出错
- 检查各模块的配置文件，mysql、redis、阿里云的AccessKey

# 启动

1. 打开nacos、redis

   `cd /usr/local/nacos/bin`,单机启动`startup.sh standlone`

   `cd /usr/local/redis` ,`redis-server redis.conf &`

2. 启动gateway网关模块和acl权限模块

3. 其他按所需功能模块启动



- aliyun-sdk-vod-upload引入报错

  ![](img/aliyun-sdk-vod-upload-jar.png)

  需要到[阿里云官网下载](https://help.aliyun.com/document_detail/51992.html?spm=a2c4g.11186623.6.1029.2dab6cecZfMGvO)，项目用的是1.4.13

  ![](img/aliyun-sdk-download.png)

  然后解压，cmd进入lib目录输入以下命令，将jar包安装到仓库。

  ```bash
  mvn install:install-file -DgroupId=com.aliyun -DartifactId=aliyun-sdk-vod-upload -Dversion=1.4.13 -Dpackaging=jar -Dfile=aliyun-java-vod-upload-1.4.13.jar
  ```

  ![](img/install%20jar%20to%20repository.png)

  

  - 前端检查nodejs环境有没有配置好，然后进入vue-admin终端执行`npm install` `npm run dev`

    中途出现提示缺什么就装什么。。。

