package cn.JInterest.commonutils;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/04  19:45
 * @Description: 定义返回状态码
 */

public interface ResultCode {

    public static Integer SUCCESS = 20000; //成功

    public static Integer ERROR = 20001; //失败
}
