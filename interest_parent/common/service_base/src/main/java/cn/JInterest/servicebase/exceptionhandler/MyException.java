package cn.JInterest.servicebase.exceptionhandler;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/10  20:37
 * @Description:
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyException extends RuntimeException{
    private Integer code ;//状态码
    private String msg;//异常信息
}
