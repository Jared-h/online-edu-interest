package cn.JInterest.servicebase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/03  21:08
 * @Description:
 */
@SpringBootApplication
public class ServiceBaseApplication {
    public static void main(String[] args){

        SpringApplication.run(ServiceBaseApplication.class,args);
    }
}
