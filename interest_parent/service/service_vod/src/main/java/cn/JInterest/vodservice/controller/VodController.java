package cn.JInterest.vodservice.controller;

import cn.JInterest.commonutils.R;
import cn.JInterest.vodservice.service.VideoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/16  20:49
 * @Description:
 */

@Api(tags = "阿里云视频点播微服务")
@RestController
@RequestMapping("/eduvod")
public class VodController {
    @Autowired
    private VideoService videoService;

    @ApiOperation(value = "vod上传视频")
    @PostMapping("upload")
    public R uploadVideo(
            @ApiParam(name = "file", value = "文件", required = true)
            @RequestParam("file") MultipartFile file) throws Exception {

        String videoId = videoService.uploadVideo(file);
        return R.ok().message("视频上传成功").data("videoId", videoId);
    }
    @ApiOperation(value = "删除云端视频")
    @DeleteMapping("{videoId}")
    public R removeVideo(@ApiParam(name = "videoId", value = "云端视频id", required = true)
                         @PathVariable String videoId){

        videoService.removeVideo(videoId);
        return R.ok().message("视频删除成功");
    }


    @ApiOperation("批量删除视频")
    @DeleteMapping("delete-batch")
    public R removeVideoList(
            @ApiParam(name = "videoIdList", value = "云端视频id", required = true)
            @RequestParam("videoIdList") List videoIdList){

        videoService.removeVideoList(videoIdList);
        return R.ok().message("视频删除成功");
    }

    @ApiOperation("获取播放凭证")
    @GetMapping("get-play-auth/{videoId}")
    public R getVideoPlayAuth(@PathVariable("videoId") String videoId)  {

        String playAuth = videoService.getVideoPlayAuth(videoId);

        return R.ok().message("获取凭证成功").data("playAuth", playAuth);
    }

}
