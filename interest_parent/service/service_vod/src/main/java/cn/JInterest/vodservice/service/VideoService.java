package cn.JInterest.vodservice.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/16  20:14
 * @Description:
 */
public interface VideoService {
    String uploadVideo(MultipartFile file);
    void removeVideo(String videoId);
    void removeVideoList(List<String> videoIdList);
    String getVideoPlayAuth(String videoId);
}
