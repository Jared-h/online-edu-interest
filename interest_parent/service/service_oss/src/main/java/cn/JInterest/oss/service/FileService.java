package cn.JInterest.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/07  20:26
 * @Description:
 */
public interface FileService {
    String uploadFileAvatar(MultipartFile file);
    void removeFileAvatar(String fileName);
}
