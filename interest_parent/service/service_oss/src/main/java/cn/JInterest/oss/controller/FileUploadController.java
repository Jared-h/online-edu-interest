package cn.JInterest.oss.controller;

import cn.JInterest.commonutils.R;
import cn.JInterest.oss.service.FileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/07  20:24
 * @Description:
 */
@Api("阿里云文件管理")
@RestController
@RequestMapping("/eduoss/file")
public class FileUploadController {

    @Autowired
    private FileService fileService;

    @ApiOperation("文件上传")
    @PostMapping("upload")
    public R upload(
            @ApiParam(name = "file",value = "文件",required = true)
            @RequestParam("file") MultipartFile file){
        String url = fileService.uploadFileAvatar(file);
        return R.ok().message("文件上传成功").data("url",url);
    }

    @ApiOperation("删除单个文件")
    @PostMapping("remove")
    public R remove(
            @ApiParam(name = "fileName",value = "文件名称",required = true)
            @RequestParam("fileName") String fileName){
        fileService.removeFileAvatar(fileName);
        return R.ok().message("文件删除成功");
    }
}
