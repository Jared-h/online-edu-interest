package cn.JInterest.staservice.controller;


import cn.JInterest.commonutils.R;
import cn.JInterest.staservice.service.StatisticsDailyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据
 * </p>
 *
 * @author JInterest
 * @since 2020-07-29
 */
@RestController
@RequestMapping("/staservice/sta")
public class StatisticsDailyController {

    @Autowired
    private StatisticsDailyService staService;

    @ApiOperation("统计某一天相关的数据")
    @PostMapping("registerCount/{day}")
    public R registerCount(@PathVariable String day) {
        staService.createStatisticsByDay(day);
        return R.ok();
    }

    @ApiOperation("图表显示，返回两部分数据，日期json数组，数量json数组")
    @GetMapping("showData/{begin}/{end}")
    public R showData(@PathVariable String begin,
                      @PathVariable String end) {
        Map<String,Object> map = staService.getChartData(begin,end);
        return R.ok().data(map);
    }

}

