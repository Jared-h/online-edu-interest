package cn.JInterest.staservice.mapper;

import cn.JInterest.staservice.entity.StatisticsDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网站统计日数据 Mapper 接口
 * </p>
 *
 * @author JInterest
 * @since 2020-07-29
 */
public interface StatisticsDailyMapper extends BaseMapper<StatisticsDaily> {

}
