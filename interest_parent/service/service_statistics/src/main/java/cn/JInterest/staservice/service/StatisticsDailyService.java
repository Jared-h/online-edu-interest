package cn.JInterest.staservice.service;

import cn.JInterest.staservice.entity.StatisticsDaily;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-29
 */
public interface StatisticsDailyService extends IService<StatisticsDaily> {
    public void createStatisticsByDay(String day);

    Map<String, Object> getChartData(String begin, String end);

}
