package cn.JInterest.staservice.service.impl;

import cn.JInterest.staservice.client.UcenterClient;
import cn.JInterest.staservice.entity.StatisticsDaily;
import cn.JInterest.staservice.mapper.StatisticsDailyMapper;
import cn.JInterest.staservice.service.StatisticsDailyService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.RandomUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务实现类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-29
 */
@Service
public class StatisticsDailyServiceImpl extends ServiceImpl<StatisticsDailyMapper, StatisticsDaily> implements StatisticsDailyService {
    @Autowired
    private UcenterClient ucenterClient;

    @Override
    public void createStatisticsByDay(String day) {
        //删除已存在的统计对象
        QueryWrapper<StatisticsDaily> dayQueryWrapper = new QueryWrapper<>();
        dayQueryWrapper.eq("date_calculated", day);
        baseMapper.delete(dayQueryWrapper);


        //获取统计信息
        Integer registerNum = (Integer) ucenterClient.registerCount(day).getData().get("countRegister");
        Integer loginNum = RandomUtils.nextInt(100, 200);//TODO
        Integer videoViewNum = RandomUtils.nextInt(100, 200);//TODO
        Integer courseNum = RandomUtils.nextInt(100, 200);//TODO

        //创建统计对象
        StatisticsDaily daily = new StatisticsDaily();
        daily.setRegisterNum(registerNum);
        daily.setLoginNum(loginNum);
        daily.setVideoViewNum(videoViewNum);
        daily.setCourseNum(courseNum);
        daily.setDateCalculated(day);

        baseMapper.insert(daily);
    }
    @ApiOperation("获取数组形式的统计信息")
    @Override
    public Map<String, Object> getChartData(String begin, String end) {

        QueryWrapper<StatisticsDaily> dayQueryWrapper = new QueryWrapper<>();
        dayQueryWrapper.between("date_calculated", begin, end);

        List<StatisticsDaily> dayList = baseMapper.selectList(dayQueryWrapper);

        List<String> dateList = new ArrayList<String>();
        List<Integer> registerNum = new ArrayList<Integer>();
        List<Integer> loginNum = new ArrayList<Integer>();
        List<Integer> videoViewNum = new ArrayList<Integer>();
        List<Integer> courseNum = new ArrayList<Integer>();

        for (StatisticsDaily daily: dayList
             ) {
            dateList.add(daily.getDateCalculated());
            registerNum.add(daily.getRegisterNum());
            loginNum.add(daily.getLoginNum());
            videoViewNum.add(daily.getVideoViewNum());
            courseNum.add(daily.getCourseNum());
        }
        Map<String, Object> map = new HashMap<>();
        map.put("dateList", dateList);
        map.put("registerNum", registerNum);
        map.put("loginNum", loginNum);
        map.put("videoViewNum", videoViewNum);
        map.put("courseNum", courseNum);

        return map;
    }


}
