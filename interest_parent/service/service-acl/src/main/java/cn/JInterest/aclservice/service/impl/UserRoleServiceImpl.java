package cn.JInterest.aclservice.service.impl;

import cn.JInterest.aclservice.entity.UserRole;
import cn.JInterest.aclservice.mapper.UserRoleMapper;
import cn.JInterest.aclservice.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-8-12
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
