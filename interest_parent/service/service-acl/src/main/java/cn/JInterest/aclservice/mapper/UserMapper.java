package cn.JInterest.aclservice.mapper;

import cn.JInterest.aclservice.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-8-12
 */
public interface UserMapper extends BaseMapper<User> {

}
