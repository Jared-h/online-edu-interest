package cn.JInterest.aclservice.service;

import cn.JInterest.aclservice.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author testjava
 * @since 2020-8-12
 */
public interface UserRoleService extends IService<UserRole> {

}
