package cn.JInterest.aclservice.service;

import cn.JInterest.aclservice.entity.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-8-12
 */
public interface RolePermissionService extends IService<RolePermission> {

}
