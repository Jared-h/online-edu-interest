package cn.JInterest.aclservice.mapper;

import cn.JInterest.aclservice.entity.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-8-12
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
