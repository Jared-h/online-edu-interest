package cn.JInterest.aclservice.service.impl;

import cn.JInterest.aclservice.entity.RolePermission;
import cn.JInterest.aclservice.mapper.RolePermissionMapper;
import cn.JInterest.aclservice.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-8-12
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
