package cn.JInterest.aclservice.mapper;

import cn.JInterest.aclservice.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-8-12
 */
public interface RoleMapper extends BaseMapper<Role> {

}
