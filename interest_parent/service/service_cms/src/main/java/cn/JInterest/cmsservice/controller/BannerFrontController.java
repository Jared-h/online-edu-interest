package cn.JInterest.cmsservice.controller;

import cn.JInterest.cmsservice.entity.CrmBanner;
import cn.JInterest.cmsservice.service.CrmBannerService;
import cn.JInterest.commonutils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/20  16:48
 * @Description:
 */

@Api(tags = "网站首页Banner列表")
@RestController
@RequestMapping("/educms/banner")
public class BannerFrontController {
    @Autowired
    private CrmBannerService bannerService;

    @ApiOperation(value = "获取首页banner")
    @GetMapping("getAllBanner")
    public R index() {
        List<CrmBanner> list = bannerService.selectAllBanner();
        return R.ok().data("bannerList", list);
    }
}
