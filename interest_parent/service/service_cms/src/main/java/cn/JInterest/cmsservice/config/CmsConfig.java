package cn.JInterest.cmsservice.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/20  16:37
 * @Description:
 */
@Configuration
@MapperScan("cn.JInterest.cmsservice.mapper")
public class CmsConfig {

}
