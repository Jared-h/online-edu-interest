package cn.JInterest.cmsservice.service.impl;

import cn.JInterest.cmsservice.client.OssClient;
import cn.JInterest.cmsservice.entity.CrmBanner;
import cn.JInterest.cmsservice.mapper.CrmBannerMapper;
import cn.JInterest.cmsservice.service.CrmBannerService;
import cn.JInterest.commonutils.R;
import cn.JInterest.servicebase.exceptionhandler.MyException;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务实现类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-20
 */
@Service
public class CrmBannerServiceImpl extends ServiceImpl<CrmBannerMapper, CrmBanner> implements CrmBannerService {

    @Autowired
    OssClient ossClient;

    @ApiOperation("查询所有banner")
    @Cacheable(value = "banner",key = "'selectIndexList'")
    @Override
    public List<CrmBanner> selectAllBanner() {

        //根据id进行降序排列，显示排列之后前两条记录
        QueryWrapper<CrmBanner> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("sort");
        //last方法，拼接sql语句
        wrapper.last("limit 4");
        List<CrmBanner> list = baseMapper.selectList(null);
        return list;
    }

    @Override
    public void pageBanner(Page<CrmBanner> pageParam, Wrapper<CrmBanner> wrapper) {
        baseMapper.selectPage(pageParam,wrapper);
    }

    @Override
    public CrmBanner getBannerById(String id) {
        return baseMapper.selectById(id);
    }

    @CacheEvict(value = "banner", allEntries=true)
    @Override
    public void saveBanner(CrmBanner banner) {
        baseMapper.insert(banner);
    }

    @CacheEvict(value = "banner", allEntries=true)
    @Override
    public void updateBannerById(CrmBanner banner) {
        baseMapper.updateById(banner);
    }

    @CacheEvict(value = "banner", allEntries=true)
    @Override
    public void removeBannerById(String id) {
        //同时把oss的图片删除
        CrmBanner crmBanner = baseMapper.selectById(id);
        if (crmBanner!=null){
            String imageUrl = crmBanner.getImageUrl();
            System.out.println(imageUrl);
            R r = ossClient.remove(imageUrl);
            if (r.getCode()==20001) throw  new MyException(20001,r.getMessage());
            System.out.println(r.toString());
        }
        baseMapper.deleteById(id);
    }

}
