package cn.JInterest.cmsservice.mapper;

import cn.JInterest.cmsservice.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author JInterest
 * @since 2020-07-20
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
