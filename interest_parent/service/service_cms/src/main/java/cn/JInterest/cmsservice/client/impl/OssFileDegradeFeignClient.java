package cn.JInterest.cmsservice.client.impl;

import cn.JInterest.cmsservice.client.OssClient;
import cn.JInterest.commonutils.R;
import org.springframework.stereotype.Component;

/**
 * @Auther: AJun
 * @version:1.0
 * @Description:熔断触发 调用实现类
 */
@Component
public class OssFileDegradeFeignClient implements OssClient {
    @Override
    public R remove(String fileName) {
        return R.error().message("连接超时,删除图片失败！");
    }
}
