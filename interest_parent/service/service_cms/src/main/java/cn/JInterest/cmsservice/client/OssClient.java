package cn.JInterest.cmsservice.client;

import cn.JInterest.cmsservice.client.impl.OssFileDegradeFeignClient;
import cn.JInterest.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/10/02  10:42
 * @Description:
 */
@FeignClient(value = "service-oss",fallback = OssFileDegradeFeignClient.class)//指定在哪个服务中调用功能
@Component
public interface OssClient {

    @PostMapping("/eduoss/file/remove")
    R remove(@RequestParam("fileName") String fileName);
}
