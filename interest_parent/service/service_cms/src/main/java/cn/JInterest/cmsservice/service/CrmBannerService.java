package cn.JInterest.cmsservice.service;

import cn.JInterest.cmsservice.entity.CrmBanner;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.cache.annotation.CacheEvict;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-20
 */
public interface CrmBannerService extends IService<CrmBanner> {

    public List<CrmBanner> selectAllBanner();

    public void pageBanner(Page<CrmBanner> pageParam, Wrapper<CrmBanner> wrapper);
    public CrmBanner getBannerById(String id);
    public void saveBanner(CrmBanner banner);
    public void updateBannerById(CrmBanner banner);
    public void removeBannerById(String id);
}
