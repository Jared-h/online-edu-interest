package cn.JInterest.cmsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/20  16:28
 * @Description:
 */
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan({"cn.JInterest"})
@EnableFeignClients
public class CmsApplication {
    public static void main(String[] args) {
        SpringApplication.run(CmsApplication.class);
    }
}
