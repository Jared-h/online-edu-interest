package cn.JInterest.eduservice.controller;


import cn.JInterest.commonutils.R;
import cn.JInterest.eduservice.entity.subject.OneSubject;
import cn.JInterest.eduservice.mapper.EduSubjectMapper;
import cn.JInterest.eduservice.service.EduSubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目
 * </p>
 *
 * @author JInterest
 * @since 2020-07-10
 */
@Api(tags="课程分类管理")
@RestController
@RequestMapping("/eduservice/subject")
//@CrossOrigin
public class EduSubjectController {

    @Autowired
    EduSubjectService eduSubjectService;

    @ApiOperation(value = "添加分类")
    @PostMapping("addSubject")
    public R addSubject(MultipartFile file){
        eduSubjectService.saveSubject(file,eduSubjectService);
        return R.ok().message("添加课程分类成功");

    }

    @ApiOperation(value = "树形结构显示")
    @GetMapping("getAllSubject")
    private R getAllSubject(){
        List<OneSubject> list= eduSubjectService.getAllSubject();
        return R.ok().data("list",list);
    }

}

