package cn.JInterest.eduservice.mapper;

import cn.JInterest.eduservice.entity.EduCourse;
import cn.JInterest.eduservice.entity.course.CourseWebVo;
import cn.JInterest.eduservice.entity.vo.CoursePublishVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author JInterest
 * @since 2020-07-11
 */
public interface EduCourseMapper extends BaseMapper<EduCourse> {
    CoursePublishVo getCoursePublishVoById(String id);

    CourseWebVo selectInfoWebById(String courseId);
}
