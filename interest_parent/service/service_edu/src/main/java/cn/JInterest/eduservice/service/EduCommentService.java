package cn.JInterest.eduservice.service;

import cn.JInterest.eduservice.entity.EduComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评论 服务类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-27
 */
public interface EduCommentService extends IService<EduComment> {

}
