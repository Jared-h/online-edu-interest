package cn.JInterest.eduservice.service;

import cn.JInterest.eduservice.entity.EduChapter;
import cn.JInterest.eduservice.entity.course.ChapterVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-11
 */
public interface EduChapterService extends IService<EduChapter> {

    List<ChapterVo> getChapterVideoByCourseId(String courseId);

    boolean removeChapterById(String id);

    Boolean removeChapterByCourseId(String courseId);
}
