package cn.JInterest.eduservice.client.impl;

import cn.JInterest.commonutils.R;
import cn.JInterest.eduservice.client.VodClient;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/18  21:43
 * @Description:熔断触发 调用实现类
 */
@Component
public class VodFileDegradeFeignClient implements VodClient {

    @Override
    public R removeVideo(String videoId) {
        return R.error().message("连接超时,删除视频失败！");
    }

    @Override
    public R removeVideoList(List videoIdList) {
        return R.error().message("连接超时,删除多个视频失败！");
    }
}
