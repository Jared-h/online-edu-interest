package cn.JInterest.eduservice.mapper;

import cn.JInterest.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author JInterest
 * @since 2020-07-11
 */
public interface EduChapterMapper extends BaseMapper<EduChapter> {

}
