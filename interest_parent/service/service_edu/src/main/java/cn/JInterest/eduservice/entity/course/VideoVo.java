package cn.JInterest.eduservice.entity.course;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/12  21:26
 * @Description:
 */
@ApiModel(value="小节信息", description="小节表单对象")
@Data
public class VideoVo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "小节id")
    private String id;
    @ApiModelProperty(value = "小节标题")
    private String title;

    @ApiModelProperty(value = "原始文件名称")
    private String videoOriginalName;

    @ApiModelProperty(value = "视频id")
    private String videoSourceId;

    @ApiModelProperty(value = "是否可以试听：0收费 1免费")
    private Boolean isFree;
}
