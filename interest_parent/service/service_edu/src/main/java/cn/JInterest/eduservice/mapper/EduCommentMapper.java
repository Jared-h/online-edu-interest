package cn.JInterest.eduservice.mapper;

import cn.JInterest.eduservice.entity.EduComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论 Mapper 接口
 * </p>
 *
 * @author JInterest
 * @since 2020-07-27
 */
public interface EduCommentMapper extends BaseMapper<EduComment> {

}
