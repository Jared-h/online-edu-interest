package cn.JInterest.eduservice.service;

import cn.JInterest.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-11
 */
public interface EduCourseDescriptionService extends IService<EduCourseDescription> {

}
