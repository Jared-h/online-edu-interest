package cn.JInterest.eduservice.config;

import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.injector.LogicSqlInjector;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/02  16:14
 * @Description: 配置类
 */

@Configuration
@MapperScan("cn.JInterest.eduservice.mapper")
public class EduConfig {

    /**
     *    逻辑删除插件  需要在实体类逻辑删除属性上加 @TableLogic
     */
    @Bean
    public ISqlInjector sqlInjector(){
        return new LogicSqlInjector();
    }

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
