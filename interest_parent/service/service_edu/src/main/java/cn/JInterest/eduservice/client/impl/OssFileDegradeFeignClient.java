package cn.JInterest.eduservice.client.impl;

import cn.JInterest.commonutils.R;
import cn.JInterest.eduservice.client.OssClient;
import org.springframework.stereotype.Component;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/18  21:45
 * @Description:熔断触发 调用实现类
 */
@Component
public class OssFileDegradeFeignClient implements OssClient {
    @Override
    public R remove(String fileName) {
        return R.error().message("连接超时,删除图片失败！");
    }
}
