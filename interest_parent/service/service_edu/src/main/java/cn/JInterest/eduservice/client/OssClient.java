package cn.JInterest.eduservice.client;

import cn.JInterest.commonutils.R;
import cn.JInterest.eduservice.client.impl.OssFileDegradeFeignClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/18  19:07
 * @Description:
 */

@FeignClient(value = "service-oss",fallback = OssFileDegradeFeignClient.class)//指定在哪个服务中调用功能
@Component
public interface OssClient {

    @PostMapping("/eduoss/file/remove")
    public R remove(@RequestParam("fileName") String fileName);
}
