package cn.JInterest.eduservice.service.impl;

import cn.JInterest.commonutils.R;
import cn.JInterest.eduservice.client.OssClient;
import cn.JInterest.eduservice.entity.*;
import cn.JInterest.eduservice.entity.course.ChapterVo;
import cn.JInterest.eduservice.entity.course.CourseQueryVo;
import cn.JInterest.eduservice.entity.course.CourseWebVo;
import cn.JInterest.eduservice.entity.vo.CourseInfoVo;
import cn.JInterest.eduservice.entity.vo.CoursePublishVo;
import cn.JInterest.eduservice.entity.vo.TeacherQuery;
import cn.JInterest.eduservice.mapper.EduCourseMapper;
import cn.JInterest.eduservice.service.EduChapterService;
import cn.JInterest.eduservice.service.EduCourseDescriptionService;
import cn.JInterest.eduservice.service.EduCourseService;
import cn.JInterest.eduservice.service.EduVideoService;
import cn.JInterest.servicebase.exceptionhandler.MyException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-11
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {

    @Autowired
    private EduCourseDescriptionService eduCourseDescriptionService;

    @Autowired
    private EduVideoService eduVideoService;

    @Autowired
    private EduChapterService eduChapterService;

    @Autowired
    private OssClient ossClient;

    private final String Bucket = "https://interest-edu.oss-cn-shenzhen.aliyuncs.com/";

    @ApiOperation(value = "保存课程基本信息")
    @Transactional
    @Override
    public String saveCourseInfoVo(CourseInfoVo courseInfoVo) {
        //将vo类的属性封装到课程实体类中
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo,eduCourse);
        int insert = baseMapper.insert(eduCourse);
        if (insert<=0){
            throw new MyException(20001,"课程信息添加失败");
        }
        //实现一对一的关系
        String cid = eduCourse.getId();
        //封装课程描述信息
        EduCourseDescription eduCourseDescription = new EduCourseDescription();
        eduCourseDescription.setId(cid);
        eduCourseDescription.setDescription(courseInfoVo.getDescription());
        boolean save = eduCourseDescriptionService.save(eduCourseDescription);
        if (!save){
            throw new MyException(20001,"课程描述信息保存失败");
        }
        return cid;
    }


    @ApiOperation(value = "获取课程基本信息")
    @Override
    public CourseInfoVo getCourseVideo(String id) {

        //最终返回的封装vo类
        CourseInfoVo courseInfoVo = new CourseInfoVo();

        //获得课程信息并封装到vo类
        EduCourse eduCourse = baseMapper.selectById(id);
        if (eduCourse==null) throw new MyException(20001,"数据不存在");
        BeanUtils.copyProperties(eduCourse,courseInfoVo);


        //获得课程描述信息
        EduCourseDescription courseDescription = eduCourseDescriptionService.getById(id);
        if (courseDescription!=null) {
            BeanUtils.copyProperties(courseDescription,courseInfoVo);
        }

        return courseInfoVo;
    }

    @ApiOperation(value = "修改课程信息")
    @Transactional
    @Override
    public void updateCourseInfoById(CourseInfoVo courseInfoVo) {
        //保存课程基本信息
        EduCourse course = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo, course);
        int resultCourseInfo = baseMapper.updateById(course);
        if(resultCourseInfo <= 0){
            throw new MyException(20001, "课程信息保存失败");
        }

        //保存课程详情信息
        EduCourseDescription courseDescription = new EduCourseDescription();
        courseDescription.setDescription(courseInfoVo.getDescription());
        courseDescription.setId(course.getId());
        boolean resultDescription = eduCourseDescriptionService.updateById(courseDescription);
        if(!resultDescription){
            throw new MyException(20001, "课程详情信息保存失败");
        }
    }

    @ApiOperation(value = "获取发布信息")
    @Override
    public CoursePublishVo getCoursePublishVoById(String id) {
        return baseMapper.getCoursePublishVoById(id);
    }

    @ApiOperation(value = "最终发布")
    @Transactional
    @Override
    public Boolean publishCourseById(String id) {
        EduCourse course = new EduCourse();
        course.setId(id);
        course.setStatus(EduCourse.COURSE_NORMAL);
        Integer count = baseMapper.updateById(course);
        return null != count && count > 0;
    }

    @ApiOperation(value = "根据条件分页查询课程信息")
    @Override
    public void pageByInfo(Page<EduCourse> pageCourse, CourseQueryVo courseQueryVo){
        //构建条件
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("gmt_create");
        //如果没有查询条件直接返回
        if (courseQueryVo == null){
            baseMapper.selectPage(pageCourse, wrapper);
            return;
        }
        // 多条件组合查询
        String title = courseQueryVo.getTitle();
        String teacherId = courseQueryVo.getTeacherId();
        String subjectParentId = courseQueryVo.getSubjectParentId();
        String subjectId = courseQueryVo.getSubjectId();
        String status = courseQueryVo.getStatus();
       // String begin = courseQueryVo.getBegin();
       // String end = courseQueryVo.getEnd();
        //判断条件值是否为空，如果不为空拼接条件
        if(!StringUtils.isEmpty(title)) {
            //构建条件
            wrapper.like("title",title);
        }
        if(!StringUtils.isEmpty(status)) {
            wrapper.eq("status",status);
        }
        if (!StringUtils.isEmpty(teacherId)){
            wrapper.eq("teacher_id",teacherId);
        }
        if (!StringUtils.isEmpty(subjectParentId)){
            wrapper.ge("subject_parent_id",subjectParentId);
        }
        if (!StringUtils.isEmpty(subjectId)){
            wrapper.ge("subject_id",subjectId);
        }
/*        if(!StringUtils.isEmpty(begin)) {
            wrapper.ge("gmt_create",begin);
        }
        if(!StringUtils.isEmpty(end)) {
            wrapper.le("gmt_create",end);
        }*/

        //调用方法实现条件查询分页
        baseMapper.selectPage(pageCourse,wrapper);
    }

    @ApiOperation(value = "删除课程所有信息")
    @Transactional
    @Override
    public boolean removeAllCourseById(String id) {
        //删除小节视频信息
        Boolean video=eduVideoService.removeVideoByCourseId(id);
        //删除章节信息
        Boolean chapter=eduChapterService.removeChapterByCourseId(id);
        //删除课程描述信息
        Boolean description=eduCourseDescriptionService.removeById(id);
        //删除云端课程封面
        EduCourse eduCourse = baseMapper.selectById(id);
        //截取封面名字,存在就删除
        String cover = eduCourse.getCover();
        if (!StringUtils.isEmpty(cover)&&cover.contains(Bucket)){
            String fileName = cover.substring(Bucket.length());
            R r = ossClient.remove(fileName);
            if (r.getCode()==20001) throw  new MyException(20001,r.getMessage());
        }
        //删除课程信息
        int result = baseMapper.deleteById(id);

        return result>0;
    }

    @ApiOperation("根据讲师id查课程，前台页面")
    @Override
    public List<EduCourse> selectByTeacherId(String teacherId) {

        QueryWrapper<EduCourse> queryWrapper = new QueryWrapper<>();

        queryWrapper.eq("teacher_id", teacherId);
        //按照最后更新时间倒序排列
        queryWrapper.orderByDesc("gmt_modified");

        List<EduCourse> courses = baseMapper.selectList(queryWrapper);
        return courses;
    }

    @ApiOperation("带条件分页查课程，前台页面")
    @Override
    public Map<String, Object> pageListWeb(Page<EduCourse> pageParam, CourseQueryVo courseQuery) {
        QueryWrapper<EduCourse> queryWrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(courseQuery.getSubjectParentId())) {
            queryWrapper.eq("subject_parent_id", courseQuery.getSubjectParentId());
        }

        if (!StringUtils.isEmpty(courseQuery.getSubjectId())) {
            queryWrapper.eq("subject_id", courseQuery.getSubjectId());
        }

        if (!StringUtils.isEmpty(courseQuery.getViewCount())) {
            queryWrapper.orderByDesc("view_count");
        }

        if (!StringUtils.isEmpty(courseQuery.getGmtCreateSort())) {
            queryWrapper.orderByDesc("gmt_create");
        }

        if (!StringUtils.isEmpty(courseQuery.getPrice())) {
            queryWrapper.orderByDesc("price");
        }

        baseMapper.selectPage(pageParam, queryWrapper);

        List<EduCourse> records = pageParam.getRecords();
        long current = pageParam.getCurrent();
        long pages = pageParam.getPages();
        long size = pageParam.getSize();
        long total = pageParam.getTotal();
        boolean hasNext = pageParam.hasNext();
        boolean hasPrevious = pageParam.hasPrevious();

        Map<String, Object> map = new HashMap<>();
        map.put("items", records);
        map.put("current", current);
        map.put("pages", pages);
        map.put("size", size);
        map.put("total", total);
        map.put("hasNext", hasNext);
        map.put("hasPrevious", hasPrevious);

        return map;
    }
    @ApiOperation("前台页面,获取课程信息并更新浏览量")
    @Override
    public CourseWebVo selectInfoWebById(String id) {
        this.updatePageViewCount(id);
        return baseMapper.selectInfoWebById(id);
    }

    @Override
    public void updatePageViewCount(String id) {
        EduCourse course = baseMapper.selectById(id);
        course.setViewCount(course.getViewCount() + 1);
        baseMapper.updateById(course);
    }
}
