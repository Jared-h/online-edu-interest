package cn.JInterest.eduservice.mapper;

import cn.JInterest.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author JInterest
 * @since 2020-07-02
 */
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
