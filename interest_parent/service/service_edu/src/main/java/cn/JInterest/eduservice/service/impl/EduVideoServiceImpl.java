package cn.JInterest.eduservice.service.impl;

import cn.JInterest.commonutils.R;
import cn.JInterest.eduservice.client.OssClient;
import cn.JInterest.eduservice.client.VodClient;
import cn.JInterest.eduservice.entity.EduVideo;
import cn.JInterest.eduservice.entity.vo.VideoInfoVo;
import cn.JInterest.eduservice.mapper.EduVideoMapper;
import cn.JInterest.eduservice.service.EduVideoService;
import cn.JInterest.servicebase.exceptionhandler.MyException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-11
 */
@Service
public class EduVideoServiceImpl extends ServiceImpl<EduVideoMapper, EduVideo> implements EduVideoService {

    @Autowired
    VodClient vodClient;


    @ApiOperation(value = "查询章节下是否有视频小节")
    @Override
    public boolean getCountByChapterId(String chapterId) {
        QueryWrapper<EduVideo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("chapter_id", chapterId);
        Integer count = baseMapper.selectCount(queryWrapper);
        return null != count && count > 0;
    }

    @ApiOperation(value = "获取视频小节信息")
    @Override
    public VideoInfoVo getVideoInfoVoById(String id) {
        //从video表中取数据
        EduVideo video = this.getById(id);
        if(video == null){
            throw new MyException(20001, "数据不存在");
        }

        //创建videoInfoForm对象
        VideoInfoVo videoInfoVo = new VideoInfoVo();
        BeanUtils.copyProperties(video, videoInfoVo);

        return videoInfoVo;
    }


    @ApiOperation(value = "保存课时基本信息")
    @Transactional
    @Override
    public void updateVideoInfoVoById(VideoInfoVo videoInfoVo) {

        EduVideo video = new EduVideo();
        BeanUtils.copyProperties(videoInfoVo, video);
        boolean result = this.updateById(video);
        if(!result){
            throw new MyException(20001, "课时信息保存失败");
        }
    }

    @ApiOperation(value = "删除单个小节视频资源")
    @Transactional
    @Override
    public boolean removeVideoById(String id) {

        //删除云端视频
        EduVideo eduVideo = baseMapper.selectById(id);
        String videoSourceId = eduVideo.getVideoSourceId();
        if(!StringUtils.isEmpty(videoSourceId)){
            vodClient.removeVideo(videoSourceId);
        }
        //删除小节信息
        Integer result = baseMapper.deleteById(id);
        return null != result && result > 0;
    }

    @ApiOperation(value = "添加小节信息")
    @Transactional
    @Override
    public boolean saveVideoInfoVo(VideoInfoVo videoInfoVo) {


        EduVideo eduVideo = new EduVideo();
        BeanUtils.copyProperties(videoInfoVo,eduVideo);
        int insert = baseMapper.insert(eduVideo);
        return insert>0;
    }

    @ApiOperation(value = "根据课程id删除")
    @Transactional
    @Override
    public Boolean removeVideoByCourseId(String courseId) {

        QueryWrapper<EduVideo> wrapper = new QueryWrapper();
        wrapper.eq("course_id",courseId);
        wrapper.select("video_source_id");
        List<EduVideo> eduVideos = baseMapper.selectList(wrapper);

        //得到所有视频列表的云端原始视频id
        List<String> videoSourceIdList = new ArrayList<>();
        for (int i = 0; i < eduVideos.size(); i++) {
            EduVideo video = eduVideos.get(i);
            String videoSourceId = video.getVideoSourceId();
            if(!StringUtils.isEmpty(videoSourceId)){
                videoSourceIdList.add(videoSourceId);
            }
        }
        //调用vod服务删除远程视频
        if(videoSourceIdList.size() > 0){
            R r = vodClient.removeVideoList(videoSourceIdList);
            if (r.getCode()==20001) throw  new MyException(20001,r.getMessage());
        }

        //删除video表的记录
        QueryWrapper<EduVideo> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.eq("course_id", courseId);
        int delete = baseMapper.delete(wrapper);
        return delete >0;
    }

}
