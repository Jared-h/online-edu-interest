package cn.JInterest.eduservice.client;

import cn.JInterest.commonutils.R;
import cn.JInterest.eduservice.client.impl.VodFileDegradeFeignClient;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/18  15:56
 * @Description:
 */
@FeignClient(value = "service-vod",fallback = VodFileDegradeFeignClient.class)//指定在哪个服务中调用功能
@Component
public interface VodClient {

    @DeleteMapping("/eduvod/{videoId}")
    public R removeVideo(@PathVariable("videoId") String videoId);

    @DeleteMapping( "/eduvod/delete-batch")
    public R removeVideoList(@RequestParam("videoIdList") List<String> videoIdList);
}
