package cn.JInterest.eduservice.controller;


import cn.JInterest.commonutils.JwtUtils;
import cn.JInterest.commonutils.R;
import cn.JInterest.eduservice.client.OrderClient;
import cn.JInterest.eduservice.entity.EduCourse;
import cn.JInterest.eduservice.entity.course.ChapterVo;
import cn.JInterest.eduservice.entity.course.CourseQueryVo;
import cn.JInterest.eduservice.entity.course.CourseWebVo;
import cn.JInterest.eduservice.entity.vo.CourseInfoVo;
import cn.JInterest.eduservice.entity.vo.CoursePublishVo;
import cn.JInterest.eduservice.service.EduChapterService;
import cn.JInterest.eduservice.service.EduCourseService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程
 * </p>
 *
 * @author JInterest
 * @since 2020-07-11
 */
@Api(tags = "课程管理")
@RestController
@RequestMapping("/eduservice/course")
//@CrossOrigin
public class EduCourseController {

    @Autowired
    private EduCourseService eduCourseService;
    @Autowired
    private EduChapterService eduChapterService;
    @Autowired
    private OrderClient orderClient;

    @ApiOperation(value ="添加课程信息")
    @PostMapping("addCourseInfo")
    public R addCourse(
            @ApiParam(name = "CourseInfoForm", value = "课程基本信息", required = true)
            @RequestBody CourseInfoVo courseInfoVo){
        String courseId = eduCourseService.saveCourseInfoVo(courseInfoVo);

        return R.ok().data("courseId",courseId);
    }

    @ApiOperation(value ="根据id获得课程信息")
    @GetMapping("getCourseInfo/{id}")
    public R getCourseInfo(
            @ApiParam(name = "id", value = "课程ID", required = true)
            @PathVariable String id){

        CourseInfoVo courseInfoVo = eduCourseService.getCourseVideo(id);
        return R.ok().data("courseInfoVo",courseInfoVo);
    }

    @ApiOperation(value = "更新课程")
    @PostMapping("updateCourseInfo")
    public R updateCourseInfoById(
            @ApiParam(name = "CourseInfoForm", value = "课程基本信息", required = true)
            @RequestBody CourseInfoVo courseInfoVo){

        eduCourseService.updateCourseInfoById(courseInfoVo);
        return R.ok();
    }

    @ApiOperation(value = "根据ID获取课程发布信息")
    @GetMapping("getCoursePublishVoById/{id}")
    public R getCoursePublishVoById(
            @ApiParam(name = "id", value = "课程ID", required = true)
            @PathVariable String id){

        CoursePublishVo courseInfoForm = eduCourseService.getCoursePublishVoById(id);
        return R.ok().data("courseInfoForm", courseInfoForm);
    }


    @ApiOperation(value = "根据id发布课程")
    @PutMapping("publishCourseById/{id}")
    public R publishCourseById(
            @ApiParam(name = "id", value = "课程ID", required = true)
            @PathVariable String id){

         eduCourseService.publishCourseById(id);
        return R.ok();
    }

    @ApiOperation(value = "条件查询带分页的方法")
    @PostMapping("pageCourseCondition/{current}/{limit}")
    public R pageCourseCondition(
            @ApiParam(name="current",value = "当前页",required = true)
            @PathVariable long current,
            @ApiParam(name="limit",value = "每页记录数",required = true)
            @PathVariable long limit,
            @RequestBody(required = false) CourseQueryVo courseQueryVo) {
        //创建page对象
        Page<EduCourse> pageCourse = new Page<>(current,limit);
        eduCourseService.pageByInfo(pageCourse,courseQueryVo);

        long total = pageCourse.getTotal();//总记录数
        List<EduCourse> records = pageCourse.getRecords(); //数据list集合
        return R.ok().data("total",total).data("rows",records);
    }

    @ApiOperation(value = "删除课程信息")
    @DeleteMapping("{id}")
    @Transactional
    public R deleteByCourseId( @ApiParam(name = "id", value = "课程ID", required = true)
                             @PathVariable String id){
        boolean flag = eduCourseService.removeAllCourseById(id);
        if (flag){
            return R.ok();
        }else{
            return R.error().message("删除失败！");
        }
    }

    @ApiOperation(value = "前台分页课程列表")
    @PostMapping(value = "{page}/{limit}")
    public R pageList(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit,

            @ApiParam(name = "courseQuery", value = "查询对象", required = false)
            @RequestBody(required = false) CourseQueryVo courseQuery){
        Page<EduCourse> pageParam = new Page<>(page, limit);
        Map<String, Object> map = eduCourseService.pageListWeb(pageParam, courseQuery);
        return  R.ok().data(map);
    }



    @ApiOperation(value = "根据ID查询课程")
    @GetMapping(value = "{courseId}")
    public R getById(
            @ApiParam(name = "courseId", value = "课程ID", required = true)
            @PathVariable String courseId, HttpServletRequest request){

        //查询课程信息和讲师信息
        CourseWebVo courseWebVo = eduCourseService.selectInfoWebById(courseId);

        //查询当前课程的章节信息
        List<ChapterVo> chapterVoList = eduChapterService.getChapterVideoByCourseId(courseId);

        Boolean buyCourse = orderClient.isBuyCourse(JwtUtils.getMemberIdByJwtToken(request), courseId);

        return R.ok().data("course", courseWebVo).data("chapterVoList", chapterVoList).data("isBuy",buyCourse);
    }

    @ApiOperation("根据课程id查询课程信息,封装到公共模块对应的实体类")
    @GetMapping("getDto/{courseId}")
    public cn.JInterest.commonutils.vo.CourseWebVoOrder getCourseInfoDto(@PathVariable String courseId) {
        CourseWebVo courseWebVo = eduCourseService.selectInfoWebById(courseId);
        cn.JInterest.commonutils.vo.CourseWebVoOrder courseInfo = new cn.JInterest.commonutils.vo.CourseWebVoOrder();
        BeanUtils.copyProperties(courseWebVo,courseInfo);
        return courseInfo;
    }
}

