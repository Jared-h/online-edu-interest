package cn.JInterest.eduservice.entity.subject;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/10  23:56
 * @Description:二级分类
 */
@ApiModel(value="二级分类对象", description="二级分类表单对象")
@Data
public class TweSubject {

    @ApiModelProperty(value = "二级分类id")
    private String id;
    @ApiModelProperty(value = "二级分类标题")
    private String title;

}
