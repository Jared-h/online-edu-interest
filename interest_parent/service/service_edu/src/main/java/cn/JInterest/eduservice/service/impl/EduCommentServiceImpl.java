package cn.JInterest.eduservice.service.impl;

import cn.JInterest.eduservice.entity.EduComment;
import cn.JInterest.eduservice.mapper.EduCommentMapper;
import cn.JInterest.eduservice.service.EduCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 评论 服务实现类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-27
 */
@Service
public class EduCommentServiceImpl extends ServiceImpl<EduCommentMapper, EduComment> implements EduCommentService {

}
