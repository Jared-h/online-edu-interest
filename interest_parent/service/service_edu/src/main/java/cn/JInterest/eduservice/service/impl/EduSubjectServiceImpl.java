package cn.JInterest.eduservice.service.impl;

import cn.JInterest.eduservice.entity.EduSubject;
import cn.JInterest.eduservice.entity.excel.ExcelSubjectData;
import cn.JInterest.eduservice.entity.subject.OneSubject;
import cn.JInterest.eduservice.entity.subject.TweSubject;
import cn.JInterest.eduservice.listenter.SubjectExcelListener;
import cn.JInterest.eduservice.mapper.EduSubjectMapper;
import cn.JInterest.eduservice.service.EduSubjectService;
import cn.JInterest.servicebase.exceptionhandler.MyException;
import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.util.BeanUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.security.auth.Subject;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-10
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    @ApiOperation(value = "添加课程分类")
    @Transactional
    @Override
    public void saveSubject(MultipartFile file, EduSubjectService eduSubjectService) {
        try {
            //1 获取文件输入流
            InputStream inputStream = file.getInputStream();

            // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
            EasyExcel.read(inputStream, ExcelSubjectData.class, new SubjectExcelListener(eduSubjectService)).sheet().doRead();
        }catch(Exception e) {
            e.printStackTrace();
            throw new MyException(20002,"添加课程分类失败");
        }
    }

    @ApiOperation(value = "获得分类")
    @Override
    public List<OneSubject> getAllSubject() {
        //获得一级分类
        QueryWrapper oneWrapper = new QueryWrapper();
        oneWrapper.eq("parent_id","0");
        List<EduSubject> subjectList1 = baseMapper.selectList(oneWrapper);
        //二级分类
        QueryWrapper tweWrapper = new QueryWrapper();
        oneWrapper.ne("parent_id","0");
        List<EduSubject> subjectList2 = baseMapper.selectList(tweWrapper);

        //最终返回的list
        List<OneSubject> finalList = new ArrayList<>();
        //封装一级分类
        for (EduSubject s1:subjectList1
             ) {
            OneSubject oneSubject=new OneSubject();
            //将对应的属性封装到一级分类实体类中
            BeanUtils.copyProperties(s1,oneSubject);
            finalList.add(oneSubject);

            //封装二级分类
            List<TweSubject> tweSubjectList = new ArrayList<>();
            for (EduSubject s2:subjectList2
                 ) {
                //如果存在二级再封装进一级
                if (s2.getParentId().equals(oneSubject.getId())){
                    TweSubject tweSubject = new TweSubject();
                    BeanUtils.copyProperties(s2,tweSubject);
                    tweSubjectList.add(tweSubject);
                }
            }
            //添加所有二级分类集合
            oneSubject.setChildren(tweSubjectList);

        }

        return finalList;
    }
}






























