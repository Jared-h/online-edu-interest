package cn.JInterest.eduservice.mapper;

import cn.JInterest.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author JInterest
 * @since 2020-07-11
 */
public interface EduVideoMapper extends BaseMapper<EduVideo> {

}
