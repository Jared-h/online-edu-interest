package cn.JInterest.eduservice.mapper;

import cn.JInterest.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程科目 Mapper 接口
 * </p>
 *
 * @author JInterest
 * @since 2020-07-10
 */
public interface EduSubjectMapper extends BaseMapper<EduSubject> {

}
