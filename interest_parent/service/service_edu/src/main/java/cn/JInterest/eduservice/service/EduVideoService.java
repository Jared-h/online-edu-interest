package cn.JInterest.eduservice.service;

import cn.JInterest.eduservice.entity.EduVideo;
import cn.JInterest.eduservice.entity.vo.VideoInfoVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-11
 */
public interface EduVideoService extends IService<EduVideo> {

    boolean getCountByChapterId(String chapterId);

    VideoInfoVo getVideoInfoVoById(String id);

    void updateVideoInfoVoById(VideoInfoVo videoInfoVo);

    boolean removeVideoById(String id);

    boolean saveVideoInfoVo(VideoInfoVo videoInfoVo);

    Boolean removeVideoByCourseId(String courseId);
}
