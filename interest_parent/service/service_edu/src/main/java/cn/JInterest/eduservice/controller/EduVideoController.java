package cn.JInterest.eduservice.controller;


import cn.JInterest.commonutils.R;
import cn.JInterest.eduservice.entity.vo.VideoInfoVo;
import cn.JInterest.eduservice.service.EduVideoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频
 * </p>
 *
 * @author JInterest
 * @since 2020-07-11
 */
@Api(tags="课程小节管理")
@RestController
@RequestMapping("/eduservice/video")
//@CrossOrigin
public class EduVideoController {

    @Autowired
    EduVideoService eduVideoService;

    @ApiOperation(value = "根据ID查询课时")
    @GetMapping("{id}")
    public R getVideInfoById(
            @ApiParam(name = "id", value = "课时ID", required = true)
            @PathVariable String id){

        VideoInfoVo videoInfoVo = eduVideoService.getVideoInfoVoById(id);
        return R.ok().data("videoInfoVo", videoInfoVo);
    }

    @ApiOperation(value = "更新课时")
    @PutMapping("{id}")
    public R updateVideoInfoVoById(
            @ApiParam(name = "VideoInfoForm", value = "课时基本信息", required = true)
            @RequestBody VideoInfoVo videoInfoVo,

            @ApiParam(name = "id", value = "课时ID", required = true)
            @PathVariable String id){

        eduVideoService.updateVideoInfoVoById(videoInfoVo);
        return R.ok();
    }

    @ApiOperation(value = "根据ID删除课时")
    @DeleteMapping("{id}")
    public R removeById(
            @ApiParam(name = "id", value = "课时ID", required = true)
            @PathVariable String id){

        boolean result = eduVideoService.removeVideoById(id);
        if(result){
            return R.ok();
        }else{
            return R.error().message("删除失败");
        }
    }

    @ApiOperation(value = "增加课时")
    @PostMapping("addVideoInfo")
    public R addVideoInfo(
            @ApiParam(name = "VideoInfoForm", value = "课时基本信息", required = true)
            @RequestBody VideoInfoVo videoInfoVo){

        boolean result = eduVideoService.saveVideoInfoVo(videoInfoVo);

        if(result){
            return R.ok();
        }else{
            return R.error().message("保存失败");
        }
    }
}

