package cn.JInterest.eduservice.entity.subject;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/10  23:55
 * @Description:一级分类
 */
@ApiModel(value="一级分类对象", description="一级分类表单对象")
@Data
public class OneSubject {

    @ApiModelProperty(value = "一级级分类id")
    private String id;
    @ApiModelProperty(value = "一级分类标题")
    private String title;

    @ApiModelProperty(value = "封装二级分类")
    private List<TweSubject> children = new ArrayList<>();
}
