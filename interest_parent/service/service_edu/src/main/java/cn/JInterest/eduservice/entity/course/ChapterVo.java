package cn.JInterest.eduservice.entity.course;

import cn.JInterest.eduservice.entity.subject.TweSubject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/12  21:16
 * @Description:
 */
@ApiModel(value="章节信息", description="章节表单对象")
@Data
public class ChapterVo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "章节id")
    private String id;
    @ApiModelProperty(value = "章节标题")
    private String title;

    @ApiModelProperty(value = "封装小节")
    private List<VideoVo> children = new ArrayList<>();
}
