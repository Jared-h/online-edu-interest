package cn.JInterest.eduservice.service;

import cn.JInterest.eduservice.entity.EduCourse;
import cn.JInterest.eduservice.entity.course.ChapterVo;
import cn.JInterest.eduservice.entity.course.CourseQueryVo;
import cn.JInterest.eduservice.entity.course.CourseWebVo;
import cn.JInterest.eduservice.entity.vo.CourseInfoVo;
import cn.JInterest.eduservice.entity.vo.CoursePublishVo;
import cn.JInterest.eduservice.entity.vo.VideoInfoVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-11
 */
public interface EduCourseService extends IService<EduCourse> {

    String saveCourseInfoVo(CourseInfoVo courseInfoVo);

    CourseInfoVo getCourseVideo(String id);

    void updateCourseInfoById(CourseInfoVo courseInfoVo);


    CoursePublishVo getCoursePublishVoById(String id);

    Boolean publishCourseById(String id);

    void pageByInfo(Page<EduCourse> pageCourse, CourseQueryVo courseQueryVo);

    boolean removeAllCourseById(String id);

    List<EduCourse> selectByTeacherId(String teacherId);

    public Map<String, Object> pageListWeb(Page<EduCourse> pageParam, CourseQueryVo courseQuery);

    CourseWebVo selectInfoWebById(String id);

    void updatePageViewCount(String id);
}
