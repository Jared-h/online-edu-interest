package cn.JInterest.eduservice.service;

import cn.JInterest.eduservice.entity.EduTeacher;
import cn.JInterest.eduservice.entity.vo.TeacherQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-02
 */
public interface EduTeacherService extends IService<EduTeacher> {

    void pageByInfo(Page pageTeacher, TeacherQuery teacherQuery);

    public Map<String, Object> pageListWeb(Page<EduTeacher> pageParam);
}
