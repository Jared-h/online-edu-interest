package cn.JInterest.eduservice.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/14  0:33
 * @Description:
 */
@ApiModel(value = "课程发布信息",description = "课程信息预览检查")
@Data
public class CoursePublishVo  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程标题")
    private String title;
    @ApiModelProperty(value = "课程封面")
    private String cover;
    @ApiModelProperty(value = "课时")
    private Integer lessonNum;
    @ApiModelProperty(value = "一级分类")
    private String subjectLevelOne;
    @ApiModelProperty(value = "二级分类")
    private String subjectLevelTwo;
    @ApiModelProperty(value = "讲师姓名")
    private String teacherName;
    @ApiModelProperty(value = "价格")
    private String price;//只用于显示
}
