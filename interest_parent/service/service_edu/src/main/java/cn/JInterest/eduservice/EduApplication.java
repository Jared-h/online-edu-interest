package cn.JInterest.eduservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/02  16:04
 * @Description:
 */

@SpringBootApplication
@ComponentScan(basePackages = {"cn.JInterest"})
@EnableDiscoveryClient
@EnableFeignClients
@EnableSwagger2
public class EduApplication {

    public static void main(String[] args){

        SpringApplication.run(EduApplication.class,args);
    }
}
