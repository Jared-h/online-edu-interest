package cn.JInterest.eduservice.controller;

import cn.JInterest.commonutils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/06  15:47
 * @Description:
 */

@Api(tags="登录管理")
@RestController
@RequestMapping("/eduservice/user")
//@CrossOrigin
public class EduLoginController {

    @ApiOperation(value = "登录接口")
    @PostMapping("login")
    public R login(){
        return R.ok().data("toke","admin");
    }

    @ApiOperation(value = "info接口")
    @GetMapping("info")
    public R info(){
        return R.ok().data("roles","[admin]").data("name","admin").data("avatar","https://www.jinterest.cn/car/2000477.jpg");
    }
}





















