package cn.JInterest.eduservice.service.impl;

import cn.JInterest.eduservice.entity.EduCourseDescription;
import cn.JInterest.eduservice.mapper.EduCourseDescriptionMapper;
import cn.JInterest.eduservice.service.EduCourseDescriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程简介 服务实现类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-11
 */
@Service
public class EduCourseDescriptionServiceImpl extends ServiceImpl<EduCourseDescriptionMapper, EduCourseDescription> implements EduCourseDescriptionService {

}
