package cn.JInterest.eduservice.client;

import cn.JInterest.commonutils.vo.UcenterMember;
import cn.JInterest.eduservice.client.impl.UcenterClientImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/27  23:25
 * @Description:
 */
@Component
@FeignClient(name="service-ucenter",fallback = UcenterClientImpl.class)
public interface UcenterClient {

    @PostMapping("/ucenterservice/member/getInfoUc/{id}")
    public UcenterMember getInfo(@PathVariable("id") String id);
}

