package cn.JInterest.eduservice.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/10  20:43
 * @Description:Excel读取数据的实体类
 */
@Data
public class ExcelSubjectData {
    @ApiParam("一级分类")
    @ExcelProperty(index = 0)
    private String oneSubjectName;

    @ApiParam("二级分类")
    @ExcelProperty(index = 1)
    private String twoSubjectName;
}