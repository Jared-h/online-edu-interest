package cn.JInterest.eduservice.mapper;

import cn.JInterest.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程简介 Mapper 接口
 * </p>
 *
 * @author JInterest
 * @since 2020-07-11
 */
public interface EduCourseDescriptionMapper extends BaseMapper<EduCourseDescription> {

}
