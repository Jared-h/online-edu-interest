package cn.jinterest;

import org.junit.Test;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/18  19:22
 * @Description:
 */
public class testSubstring {

    @Test
    public void test(){
        String name = "https://interest-edu.oss-cn-shenzhen.aliyuncs.com/2020/07/18/e802b4a6c71e41f3bfc47503f7154a4d8c04bb29bbd770555aef3f60a55f755c.jpg";
        String trim = "https://interest-edu.oss-cn-shenzhen.aliyuncs.com/";
        int length = trim.length();
        String substring = name.substring(50);
        System.out.println(substring);
    }

}
