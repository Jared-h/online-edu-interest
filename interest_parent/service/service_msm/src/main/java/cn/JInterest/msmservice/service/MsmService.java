package cn.JInterest.msmservice.service;

import java.util.Map;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/21  23:09
 * @Description:
 */
public interface MsmService {
    public boolean send(String PhoneNumbers, String templateCode, Map<String,Object> param) ;
}
