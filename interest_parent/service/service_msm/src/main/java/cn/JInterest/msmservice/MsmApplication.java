package cn.JInterest.msmservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/21  23:06
 * @Description:
 */
@ComponentScan({"cn.JInterest"})
@EnableDiscoveryClient
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)//取消数据源自动配置
public class MsmApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsmApplication.class);
    }
}

