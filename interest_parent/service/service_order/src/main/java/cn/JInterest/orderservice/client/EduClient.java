package cn.JInterest.orderservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/28  17:09
 * @Description:
 */

@Component
@FeignClient("service-edu")
public interface EduClient {
    //根据课程id查询课程信息
    @GetMapping("/eduservice/course/getDto/{courseId}")
    public cn.JInterest.commonutils.vo.CourseWebVoOrder getCourseInfoDto(@PathVariable("courseId") String courseId);
}