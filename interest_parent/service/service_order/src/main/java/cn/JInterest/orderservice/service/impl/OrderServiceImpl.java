package cn.JInterest.orderservice.service.impl;

import cn.JInterest.commonutils.vo.CourseWebVoOrder;
import cn.JInterest.commonutils.vo.UcenterMember;
import cn.JInterest.orderservice.client.EduClient;
import cn.JInterest.orderservice.client.UcenterClient;
import cn.JInterest.orderservice.entity.Order;
import cn.JInterest.orderservice.mapper.OrderMapper;
import cn.JInterest.orderservice.service.OrderService;
import cn.JInterest.orderservice.util.OrderNoUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-28
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Autowired
    private EduClient eduClient;

    @Autowired
    private UcenterClient ucenterClient;

    @ApiOperation("//创建订单")
    @Override
    public String saveOrder(String courseId, String memberIdByJwtToken) {
        //远程调用课程服务，根据课程id获取课程信息
        CourseWebVoOrder courseDto = eduClient.getCourseInfoDto(courseId);

        //远程调用用户服务，根据用户id获取用户信息
        UcenterMember ucenterMember = ucenterClient.getInfo(memberIdByJwtToken);

        //创建订单
        Order order = new Order();
        order.setOrderNo(OrderNoUtil.getOrderNo());
        order.setCourseId(courseId);
        order.setCourseTitle(courseDto.getTitle());
        order.setCourseCover(courseDto.getCover());
        order.setTeacherName(courseDto.getTeacherName());
        order.setTotalFee(courseDto.getPrice());
        order.setMemberId(memberIdByJwtToken);
        order.setMobile(ucenterMember.getMobile());
        order.setNickname(ucenterMember.getNickname());
        order.setStatus(0); // 支付状态 1 已支付 0 未支付
        order.setPayType(1);//支付类型  1 微信 0 支付宝
        baseMapper.insert(order);

        return order.getOrderNo();
    }

}
