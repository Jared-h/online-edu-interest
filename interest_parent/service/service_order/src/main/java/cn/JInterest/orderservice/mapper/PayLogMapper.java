package cn.JInterest.orderservice.mapper;

import cn.JInterest.orderservice.entity.PayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付日志表 Mapper 接口
 * </p>
 *
 * @author JInterest
 * @since 2020-07-28
 */
public interface PayLogMapper extends BaseMapper<PayLog> {

}
