package cn.JInterest.orderservice.mapper;

import cn.JInterest.orderservice.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author JInterest
 * @since 2020-07-28
 */
public interface OrderMapper extends BaseMapper<Order> {

}
