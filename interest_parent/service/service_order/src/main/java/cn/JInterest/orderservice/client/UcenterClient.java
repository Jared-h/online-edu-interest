package cn.JInterest.orderservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @Auther: AJun
 * @version:1.0
 * @Date: 2020/07/28  17:10
 * @Description:
 */
@Component
@FeignClient("service-ucenter")
public interface UcenterClient {
    //根据课程id查询课程信息
    @PostMapping("/ucenterservice/member/getInfoUc/{id}")
    public cn.JInterest.commonutils.vo.UcenterMember getInfo(@PathVariable("id") String id);
}
