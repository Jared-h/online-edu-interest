package cn.JInterest.ucenterservice.service;

import cn.JInterest.ucenterservice.entity.UcenterMember;
import cn.JInterest.ucenterservice.entity.vo.LoginInfoVo;
import cn.JInterest.ucenterservice.entity.vo.LoginVo;
import cn.JInterest.ucenterservice.entity.vo.RegisterVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author JInterest
 * @since 2020-07-22
 */
public interface UcenterMemberService extends IService<UcenterMember> {

    String login(LoginVo loginVo);

    void register(RegisterVo registerVo);

    LoginInfoVo getLoginInfo(String memberId);

    UcenterMember getOpenIdMember(String openid) ;

    Integer countRegisterByDay(String day);
}
