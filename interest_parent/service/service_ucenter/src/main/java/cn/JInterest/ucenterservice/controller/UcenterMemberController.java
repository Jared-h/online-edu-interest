package cn.JInterest.ucenterservice.controller;


import cn.JInterest.commonutils.JwtUtils;
import cn.JInterest.commonutils.R;
import cn.JInterest.servicebase.exceptionhandler.MyException;
import cn.JInterest.ucenterservice.entity.UcenterMember;
import cn.JInterest.ucenterservice.entity.vo.LoginInfoVo;
import cn.JInterest.ucenterservice.entity.vo.LoginVo;
import cn.JInterest.ucenterservice.entity.vo.RegisterVo;
import cn.JInterest.ucenterservice.service.UcenterMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author JInterest
 * @since 2020-07-22
 */
@Api(tags = "用户登录注册中心")
@RestController
@RequestMapping("/ucenterservice/member")
//@CrossOrigin
public class UcenterMemberController {

    @Autowired
    private UcenterMemberService memberService;

    @ApiOperation(value = "会员登录")
    @PostMapping("login")
    public R login(@RequestBody LoginVo loginVo) {
        String token = memberService.login(loginVo);
        return R.ok().data("token", token);
    }


    @ApiOperation(value = "会员注册")
    @PostMapping("register")
    public R register(@RequestBody RegisterVo registerVo){
        memberService.register(registerVo);
        return R.ok();
    }

    @ApiOperation(value = "根据token获取登录信息")
    @GetMapping("auth/getLoginInfo")
    public R getLoginInfo(HttpServletRequest request){
        try {
            String memberId = JwtUtils.getMemberIdByJwtToken(request);
            LoginInfoVo loginInfoVo = memberService.getLoginInfo(memberId);
            return R.ok().data("item", loginInfoVo);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException(20001,"error");
        }
    }
    @ApiOperation(value = "根据id获取用户信息,评论相关")
    @PostMapping("getInfoUc/{id}")
    public cn.JInterest.commonutils.vo.UcenterMember getInfo(@PathVariable String id) {
        //根据用户id获取用户信息
        UcenterMember ucenterMember = memberService.getById(id);
        if(ucenterMember==null){

            throw new MyException(20001,"获取用户信息失败");
        }
        cn.JInterest.commonutils.vo.UcenterMember member = new cn.JInterest.commonutils.vo.UcenterMember();
        BeanUtils.copyProperties(ucenterMember,member);
        return member;
    }

    @ApiOperation("统计日注册人数")
    @GetMapping(value = "countregister/{day}")
    public R registerCount(
            @PathVariable String day){
        Integer count = memberService.countRegisterByDay(day);
        return R.ok().data("countRegister", count);
    }
}

