#!/usr/bin/env bash
# dockerfile文件存放路径
BASE_PATH=/home/online-edu-package/admin/lib
#docker 镜像/容器名字或者jar名字 这里都命名为这个
GATEWAY=gateway
ACL=acl
CMS=cms
EDU=edu
OSS=oss
MSM=msm
ORDER=order
STATISTICS=statistics
UCEBTER=ucenter
VOD=vod
#时间
DATE=`date +%Y%m%d%H%M`
function build(){
    echo "开始构建镜像"
	cd $BASE_PATH
	docker build -f Dockerfile-$GATEWAY -t $GATEWAY .
	echo "----1--------$GATEWAY 镜像构建完成------------"

	docker build -f Dockerfile-$ACL -t $ACL .
	echo "----2--------$ACL 镜像构建完成------------"

	docker build -f Dockerfile-$CMS -t $CMS .
	echo "----3--------$CMS 镜像构建完成------------"

	docker build -f Dockerfile-$EDU -t $EDU .
	echo "-----4-------$EDU 镜像构建完成------------"

	docker build -f Dockerfile-$OSS -t $OSS .
	echo "-----5-------$OSS 镜像构建完成------------"

	docker build -f Dockerfile-$MSM -t $MSM .
	echo "-----6-------$MSM 镜像构建完成------------"

	docker build -f Dockerfile-$ORDER -t $ORDER .
	echo "-----7-------$ORDER 镜像构建完成------------"

	docker build -f Dockerfile-$STATISTICS -t $STATISTICS .
	echo "-----8-------$STATISTICS 镜像构建完成------------"

	docker build -f Dockerfile-$UCEBTER -t $UCEBTER .
	echo "------9------$UCEBTER 镜像构建完成------------"

	docker build -f Dockerfile-$VOD -t $VOD .
	echo "------10------$VOD 镜像构建完成------------"

	docker images
}
build
