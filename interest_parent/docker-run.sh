#!/usr/bin/env bash
#操作/项目路径(Dockerfile存放的路径)
BASE_PATH=/home/online-edu-package/admin/lib
#docker 镜像/容器名字或者jar名字 这里都命名为这个
GATEWAY=gateway
ACL=acl
CMS=cms
EDU=edu
OSS=oss
MSM=msm
ORDER=order
STATISTICS=statistics
UCEBTER=ucenter
VOD=vod
# 端口号
GATEWAY_port=8222
ACL_port=8009
CMS_port=8004
EDU_port=8001
OSS_port=8002
MSM_port=8005
ORDER_port=8007
STATISTICS_port=8008
UCEBTER_port=8150
VOD_port=8003

#时间
DATE=`date +%Y%m%d%H%M`
function run(){
	echo "开始时间>>>>>>$DATE"
	cd BASE_PATH
	echo "开始运行$GATEWAY 镜像"
	docker run -p $GATEWAY_port:$GATEWAY_port --name $GATEWAY -d $GATEWAY
	echo "====1============$GATEWAY 已经运行============"

	echo "开始运行$ACL 镜像"
	docker run -p $ACL_port:$ACL_port --name $ACL -d $ACL
	echo "=====2===========$ACL 已经运行============"

	echo "开始运行$CMS 镜像"
	docker run -p $CMS_port:$CMS_port --name $CMS -d $CMS
	echo "=====3===========$CMS 已经运行============"

	echo "开始运行$EDU 镜像"
	docker run -p $EDU_port:$EDU_port --name $EDU -d $EDU
	echo "=====4===========$EDU 已经运行============"

	echo "开始运行$OSS 镜像"
	docker run -p $OSS_port:$OSS_port --name $OSS -d $OSS
	echo "=====5===========$OSS 已经运行============"

	echo "开始运行$MSM 镜像"
	docker run -p $MSM_port:$MSM_port --name $MSM -d $MSM
	echo "=====6===========$MSM 已经运行============"

	echo "开始运行$ORDER 镜像"
	docker run -p $ORDER_port:$ORDER_port --name $ORDER -d $ORDER
	echo "======7==========$ORDER 已经运行============"

	echo "开始运行$STATISTICS 镜像"
	docker run -p $STATISTICS_port:$STATISTICS_port --name $STATISTICS -d $STATISTICS
	echo "======8==========$STATISTICS 已经运行============"

	echo "开始运行$UCEBTER 镜像"
	docker run -p $UCEBTER_port:$UCEBTER_port --name $UCEBTER -d $UCEBTER
	echo "=======9=========$UCEBTER 已经运行============"

	echo "开始运行$VOD 镜像"
	docker run -p $VOD_port:$VOD_port --name $VOD -d $VOD
	echo "======10==========$VOD 已经运行============"


	docker ps -a
	echo "结束时间>>>>>>$DATE"
}
#运行
run
