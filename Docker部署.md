# 项目部署

##  环境准备

1. jdk11
2. node
3. nginx
4. nacos

因为配置使用的是阿里云的mysql和redis服务，服务器上就不需要配置了。

## 部署

---

[docker学习参考](https://blog.csdn.net/HIHE_i/article/details/108971830)

TODO:部署到多台虚拟机

   1. mvn package

      - **注意：yaml配置文件不要有空格或错误的注释**

   2. 将lib目录下的文件打包，发到linux服务器

   3. nacos配置中心配置相关配置文件，nacos需要检查防火墙端口有没有打开

      - systemctl status firewalld   查看防火墙状态
      - service firewalld  start  打开防火墙 
      - firewall-cmd --query-port=8848/tcp    查看端口是否打开
      - firewall-cmd --zone=public --add-port=80/tcp --permanent   （--permanent永久生效，没有此参数重启后失效）
      - firewall-cmd --reload   重新载入
      - firewall-cmd --query-port=8848/tcp    查看
      - systemctl stop firewalld  最后关闭防火墙

      访问 ip：8848/

   4. 修改脚本文件的`BASE_PATH`=lib解压的路径

   5. 构建镜像，启动,启动前记得打开nacos

      ```she
      sh docker-build-images.sh
      sh docker-run.sh
      ```

   6. 打开前端，访问测试

      ```shell
      # nginx
      ./nginx -c /home/online-edu-package/front/nginx/online-edu.conf
      ./nginx -c /home/online-edu-package/front/nginx/online-edu-admin.conf
      # nuxt
      cd /home/online-edu-package/front/nuxt/
      npm start
      ```

   7. 全部打包到一个docker启动。

```shell

docker build -t online-edu -f ./Dockerfile .

docker run -d --name online-edu -p 8001:8001 -p 8002:8002 -p 8003:8003 -p 8004:8004 -p 8005:8005 -p 8008:8008 -p 8007:8007 -p 8009:8009 -p 8150:8150 -p 8222:8222 online-edu 

```

