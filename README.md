# 在线教育项目

> B2C模式（Business To Customer）

## 系统前台

---

### 功能模块

1. 首页数据显示
2. 讲师列表和详情
3. 课程列表和课程详情
   - 视频在线播放
4. 登录和注册功能
5. 微信扫码登录
6. 微信扫码支付



### 技术栈

- vue+element-ui+axios+node js...



## 系统后台

---

### 功能模块

1. 讲师管理模块
2. 课程分类管理模块
3. 课程管理模块
4. oss管理模块
   - 图片
   - 视频
5. 统计分析模块
6. 订单管理
7. banner管理
8. 权限管理



### 技术栈

- SpringBoot、SpringCloud、MybatisPlus、Spring Security
- redis、maven、jwt、OAuth2、easyExcel
- 阿里云（oss，视频点播，短信服务）、微信支付登录
- docker、Jenkins、git


