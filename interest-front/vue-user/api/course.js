import request from '@/utils/request'
export default {
  //条件分页课程查询的方法
  getPageList(page, limit, searchObj) {
    return request({
      url: `/eduservice/course/${page}/${limit}`,
      method: 'post',
      data: searchObj
    })
  },
  //查询所有分类的方法
  getAllSubject() {
    return request({
      url: '/eduservice/subject/getAllSubject',
      method: 'get'
    })
  },
  getById(courseId) {
    return request({
      url: `/eduservice/course/${courseId}`,
      method: 'get'
    })
  }
}
