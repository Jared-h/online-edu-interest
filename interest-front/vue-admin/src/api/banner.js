import request from '@/utils/request'
const api_name = '/educms/banneradmin'

export default {
  getBanner(page, limit) {
    return request({
      url: `${api_name}/pageBanner/${page}/${limit}`,
      method: 'get',
    })
  },

  addBanner(crmBanner) {
    return request({
      url: `${api_name}/addBanner`,
      method: 'post',
      data: crmBanner
    })
  },

  getBannerById(id) {
    return request({
      url: `${api_name}/get/${id}`,
      method: 'get',
    })
  },

  updateBanner(crmBanner) {
    return request({
      url: `${api_name}/update`,
      method: 'put',
      data : crmBanner
    })
  },

  removeBannerById(id) {
    return request({
      url: `${api_name}/remove/${id}`,
      method: 'delete',
    })
  }
}

