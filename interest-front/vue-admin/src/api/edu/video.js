import request from '@/utils/request'
const api_name = '/eduservice/video'

export default {

  //添加小节
  addVideo(videoInfo) {
    return request({
      url: `${api_name}/addVideoInfo`,
      method: 'post',
      data: videoInfo
    })
  },

  getVideoInfoById(id) {
    return request({
      url: `${api_name}/${id}`,
      method: 'get'
    })
  },

  updateVideoInfoById(videoInfo) {
    return request({
      url: `${api_name}/${videoInfo.id}`,
      method: 'put',
      data: videoInfo
    })
  },
  //删除小节
  removeById(id) {
    return request({
      url: `${api_name}/${id}`,
      method: 'delete'
    })
  }
}
