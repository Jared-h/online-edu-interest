import request from '@/utils/request'

const api_name = '/eduservice/chapter'

export default {

  getAllChapterVideo(courseId) {
    return request({
      url: `${api_name}/getChapterVideo/`+courseId,
      method: 'get'
    })
  },
  removeById(id) {
    return request({
      url: `${api_name}/removeChapterById/${id}`,
      method: 'delete'
    })
  },

  addChapter(chapter) {
    return request({
      url: `${api_name}/addChapter`,
      method: 'post',
      data: chapter
    })
  },

  getById(id) {
    return request({
      url: `${api_name}/getChapterById/${id}`,
      method: 'get'
    })
  },

  updateChapter(chapter) {
    return request({
      url: `${api_name}/updateChapterById/${chapter.id}`,
      method: 'put',
      data: chapter
    })
  }
}
