import request from '@/utils/request'
const api_name = '/eduservice/course'

export default {
  //1 课程分类列表
  addCourse(courseInfo) {
    return request({
      url: `${api_name}/addCourseInfo`,
      method: 'post',
      data:courseInfo
    })
  },
  //2 查询所有讲师
  getListTeacher() {
    return request({
      url: `/eduservice/teacher/findAll`,
      method: 'get'
    })
  },
  //3 根据id查询课程信息
  getCourseInfoById(id) {
    return request({
      url: `${api_name}/getCourseInfo/${id}`,
      method: 'get'
    })
  },
  //4 修改课程信息
  updateCourseInfo(courseInfo) {
    return request({
      url: `${api_name}/updateCourseInfo`,
      method: 'post',
      data: courseInfo
    })
  } ,
  //5 预览课程列表信息
  getListCourse(id) {
    return request({
      url: `${api_name}/getCoursePublishVoById/${id}`,
      method: 'get'
    })
  },
  //6 课程最终发布
  publishCourse(id) {
    return request({
      url: `${api_name}/publishCourseById/${id}`,
      method: 'put'
    })
  },

  //7 current当前页 limit每页记录数 courseQuery条件对象
  getCourseListPage(current,limit,courseQuery) {
    return request({

      url: `${api_name}/pageCourseCondition/${current}/${limit}`,
      method: 'post',
      data: courseQuery
    })
  },
  //8 删除课程
  deleteByCourseId(id){
    return request({
      url: `${api_name}/${id}`,
      method: 'delete',
    })

  }

}
